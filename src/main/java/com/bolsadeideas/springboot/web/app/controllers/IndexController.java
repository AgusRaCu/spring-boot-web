package com.bolsadeideas.springboot.web.app.controllers;


import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bolsadeideas.springboot.web.app.models.Usuario;

import java.util.List;


@Controller
@RequestMapping("/app")
public class IndexController {
	
	@RequestMapping(value= {"/index","","/home"}, method =RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("titulo","Hola Spring Framework");
		return "index";
	}
	
	@GetMapping("/perfil")
		public String perfil(Model model) {
			Usuario usuario = new Usuario();
			usuario.setNombre("Agustín");
			usuario.setApellido("Ramis Cucchiarelli");
			usuario.setEmail("ramisagustin@gmail.com");
			
			
			model.addAttribute("usuario", usuario);
			model.addAttribute("titulo", "Perfil del usuario: ".concat(usuario.getNombre()));
			return "perfil";
		}
	
	
	@GetMapping("/listar")
	public String listar(Model model) {
		
		model.addAttribute("titulo","Listado de usuarios");
		
		return "listar";
	}
	
	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuarios(){
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(new Usuario("Andres","Guzman","andres@correo.com"));
		usuarios.add(new Usuario("Juan","Miguel","juan@correo.com"));
		usuarios.add(new Usuario("Ernesto","Perotti","Ernesto@correo.com"));
		return usuarios;
	}
	
}
